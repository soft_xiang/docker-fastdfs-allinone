#!/bin/bash
# author:xsj
echo -e "可能的IP地址为:\n`ip addr | grep 'inet'| grep -v '127.0.0.1'|grep -v 'docker' | awk '{ print $2}' | awk -F '/' '{print $1}'
`"
read -r -p $'==========此脚本用于测试fastdfs是否安装成功,如果修改了fastdfs或web端口信息，请修改本脚本==========\n==========请输入fastdfs服务的IP地址(一般为本机局域网IP地址，不能使用127.0.0.1)========== \n' input

echo "-----测试上传开始-----"
java -jar fastdfstest.jar "$input:web_port" ./test.txt
echo -e "-----http访问路径为:http://$input:web_port/group1/文件路径-----\n可以使用:curl http://$input:web_port/group1/文件路径\n测试访问"
