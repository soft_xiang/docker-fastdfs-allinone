# docker-fastdfs-allinone

#### Description
fastdfs5.11的dockerfile
fastdfs:5.11、nginx、fastdfs-nginx-module整合到一个docker镜像中
fastdfs tracker_server、storage_server都只有一个
适用于需要fastdfs文件服务但又不需要集群部署，或者快速开始
包含一个java的测试脚本
之所以有这个项目，是停止更新的fastdfs又开始更新了，没有时间测试新版本，只能重新构建旧版本

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
